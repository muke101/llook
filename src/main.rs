use std::env;
use utf16string::{WStr, WString, WStrChars, BE};
use std::fs::File;
use std::collections::HashMap;
use std::process::Command;
use std::io::{self, Write, BufRead};
use std::path::Path;
use std::error::Error;
use inkwell::context::Context;
use inkwell::module::Module;
extern crate json;
use json::JsonValue;
mod backend;

struct OpenFile<'ctx> {
    URI: String,
    lines: Vec<WString<BE>>, //TODO: derive from ['text']
    module: Module<'ctx>,
    ir: backend::IR<'ctx>,
    synced: bool
}

struct ServerState<'ctx>  {
    client_process: Option<i32>,
    cancelled_requests: Vec<i32>,
    open_files: HashMap<String, OpenFile<'ctx>>,
    ignored_requests: Vec<&'static str>,
    ctx: &'ctx Context
}

fn initialise(request: JsonValue, state: &mut ServerState) -> JsonValue {
    state.ignored_requests = vec!["initialized", "textDocument/willSave", "textDocument/willSaveWaitUntil"];
    let mut id: Option<i32> = None;
    match request   {
        JsonValue::Object(_) => {
            if !(request["method"] == "initialize") {
                if request["method"] == "exit" { std::process::exit(0); }
                else {
                    return json::object!{
                        id: null,
                        error: json::object!{
                            code: -32002,
                            message: "Request made before initialisation."
                        }
                    };
                }
            }
            if !request["params"]["processId"].is_null() {
                let proc_id: f64 = request["params"]["processId"].as_number().unwrap().into();
                state.client_process = Some(proc_id as i32);
            }
            if !request["id"].is_null() { id = request["id"].as_i32(); }
        }
        _ => { panic!("Request isn't an object!"); }
    }
    let server_capabilities = json::object!{
        positionEncoding: "utf-16", //TODO: actually have to conform to this :(
        textDocumentSync: json::object!{
            openClose: true,
            change: 1,
            willSave: false,
            willSaveWaitUntil: false,
            save: json::object!{
                includeText: true
            }
        },
        hoverProvider: true,
        declarationProvider: true,
        definitionProvider: true,
        referencesProvider: true,
        documentHighlightProvider: true,
        //typeDefinitionProvider: true, //TODO: jump to struct definitions
        //implementationProvider: true, //TODO: jump to function definition, i think
        //documentSymbolProvider: true, //TODO: list functions and declarations in file
        //colorProvider: true, //TODO: look into what this actually does
    };
    return json::object!{
        id: id,
        result: json::object!{
            method: "initialized",
            capabilities: server_capabilities,
            serverInfo: json::object!{
                name: "lls",
                version: "0.1"
            }
        }
    };
}

//TODO: handle IR with errors
fn open_file<'ctx>(URI: &str, text: &str, ctx: &'ctx Context) -> OpenFile<'ctx>   {
    let lines = text.lines().map(String::from).collect::<Vec<_>>();

    // can only parse from bitcode so massive hack for now
    // to just assemble the IR to .bc then parse that
    // maybe this'd be passable if touching disk were avoided
	let bc_path = URI.split(".").collect::<Vec<&str>>()[0];
	let bc_path = format!("{}{}", bc_path, ".bc");
    Command::new("llvm-as").arg(URI);
    let p = Path::new(&bc_path);
    let module = Module::parse_bitcode_from_path(p, ctx).unwrap();
    Command::new("rm -f").arg(bc_path);

    let ir = backend::IR::new(&lines, &module);

    let lines_utf16 = text.lines().map(WString::from).collect::<Vec<_>>();

    return OpenFile { URI: URI.to_string(), lines: lines_utf16, module, ir, synced: true };
}

fn dispatch_request<'a, 'ctx>(request: JsonValue, state: &'a mut ServerState<'ctx>) -> Option<JsonValue>    {
    if let JsonValue::Object(_) = request   {
        match request["method"].as_str().unwrap()   {
            "textDocument/didOpen" => {
                let URI = request["params"]["textDocument"]["URI"].as_str().unwrap();
                let text = request["params"]["textDocument"]["text"].as_str().unwrap();
                let file = open_file(URI, text, state.ctx);
                if state.open_files.insert(String::from(URI), file).is_some() { panic!("File opened twice!"); }
                None
            },
            "textDocument/didChange" => {
                let URI = request["params"]["textDocument"]["URI"].as_str().unwrap();
                let file = &mut state.open_files.get_mut(URI);
                if let Some(file) = file { file.synced = false; }
                else { panic!("didChange on unopened file!"); }
                None
            },
            "textDocument/didSave" => {
                let URI = request["params"]["textDocument"]["URI"].as_str().unwrap();
                let file = state.open_files.remove(URI);
                let text = request["params"]["contentChanges"]["text"].as_str().unwrap();
                if let Some(file) = file { state.open_files.insert(String::from(URI), open_file(URI, text, state.ctx)); }
                else { panic!("didSave on unopened file!"); }
                None
            },
            "textDocument/didClose" => {
                let URI = request["params"]["textDocument"]["URI"].as_str().unwrap();
                let file = state.open_files.remove(URI);
                if file.is_none() { panic!("didClose on unopend file!"); }
                None
            },
            "textDocument/definition" => {
                let URI = request["params"]["textDocument"]["URI"].as_str().unwrap();
                let file = &state.open_files.get(URI);
                if let Some(file) = file {
                    let line_number = request["params"]["position"]["line"].as_u32().unwrap() as usize;
                    let char_indx = request["params"]["position"]["character"].as_u32().unwrap() as usize;
                    let line = &file.lines[line_number];
                    let name_start = line.chars().take(char_indx+1).collect::<Vec<_>>().iter().rev().position(|&c| c == '%');
                    let name_end = line.chars().skip(char_indx+1).position(|c| c == ' ' || c == ',');
                    if name_start.is_none() || name_end.is_none() { return None; }
                    let name_start = char_indx - name_start.unwrap(); //position on .rev gives the index from the right
                    let ssa_name = line.chars().skip(name_start).take(name_end.unwrap() - name_start).collect::<String>();
                    let inst = file.ir.name_map.get(&ssa_name).unwrap();
                    let target_line_number = file.ir.ir_map.get(inst).unwrap();
                    let target_line = &file.lines[*target_line_number];
                    //TODO: get starting character by skipping whitespace
                }
                else { panic!("definition on unopened file!"); }
                None
                //find ssa name at cursor position
                //query llvm for IR object, return line number
            }
            "shutdown" => {
                std::process::exit(1);
            }
            _ => { if state.ignored_requests.contains(&request["method"].as_str().unwrap()) { None } else { panic!("Unknown request method: {}", request["method"].as_str().unwrap()); } }
        }
    }
    else { panic!("Request is not a Json object."); }
}

fn get_request() -> JsonValue  {
    let mut length_input = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut length_input);
    let content_length_str = length_input.split(' ').collect::<Vec<_>>()[1];
    let content_length = content_length_str[0..content_length_str.len()-2].parse::<usize>().unwrap();
    let mut json_input = String::new();
    json_input.reserve(content_length);
    let mut read_bytes = 0;
    while read_bytes <= content_length {
        match stdin.read_line(&mut json_input) {
            Ok(read) => { read_bytes += read; }
            Err(error) => { eprintln!("Error: {error}"); }
        }
    }
    let indx = json_input.find('{').unwrap();
    return json::parse(&json_input[indx..]).unwrap();
}

fn send_response(content: JsonValue, stdout: &mut std::io::StdoutLock){
    //let mut response = "Content-Length ".to_owned();
    let mut response = json::stringify(content);
    //response.push_str(format!("{}\r\n\r\n", json_string.as_bytes().len().to_string()).as_str());
    //response.push_str(&json_string);
    response.push('\n');
    eprintln!("Output: {}", response);
    stdout.write_all(response.as_bytes());
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() > 1 && args[1] != "stdio"   {
        println!("can't handle pipe or socket io yet");
    }

    let mut stdout = io::stdout().lock();
    let ctx = Context::create();
    let mut state = ServerState { client_process: None, cancelled_requests: vec![], ignored_requests: vec![], open_files: HashMap::new(), ctx: &ctx };
    let request = get_request();
    let response = initialise(request, &mut state);
    send_response(response, &mut stdout);

    loop  {
        let request = get_request();
        let response = dispatch_request(request, &mut state);
        if let Some(r) = response { send_response(r, &mut stdout); }
    }

    Ok(())
}
