use core::fmt;
use std::ffi::CStr;
use std::os::raw::c_char;
use std::path::Path;
use inkwell::module::Module;
use inkwell::context::Context;
use inkwell::values::*;
use inkwell::basic_block::BasicBlock;
use std::collections::HashMap;
use regex::Regex;
use either::Either;

// pub struct TextData {
//     pub line_number: usize,
//     pub name: String
// }

// impl fmt::Display for TextData {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result  {
//         let name = match self.name.len()  {
//             0 => "<nameless instruction>",
//             _ => self.name.as_str(),
//         };
//         write!(f, "{}, {}", self.line_number, name)
//     }
// }

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub enum LLVMValue<'ctx>  {
    Basic(BasicValueEnum<'ctx>),
    Instruction(InstructionValue<'ctx>),
    BasicBlock(BasicBlock<'ctx>),
    Function(FunctionValue<'ctx>),
}

impl<'ctx> LLVMValue<'ctx> {
    fn get_first_use(&self) -> Option<BasicValueUse>    {
        match self  {
            LLVMValue::Basic(val) => val.get_first_use(),
            LLVMValue::Instruction(val) => val.get_first_use(),
            LLVMValue::BasicBlock(val) => val.get_first_use(),
            LLVMValue::Function(_) => panic!("function uses unimplemented")
        }
    }

    pub fn as_instruction(&self) -> Option<InstructionValue>    {
        match self  {
            LLVMValue::Instruction(val) => Some(*val),
            LLVMValue::Basic(val) => match val  {
                BasicValueEnum::ArrayValue(val) => val.as_instruction(),
                BasicValueEnum::IntValue(val) => val.as_instruction(),
                BasicValueEnum::FloatValue(val) => val.as_instruction(),
                BasicValueEnum::PointerValue(val) => val.as_instruction(),
                BasicValueEnum::StructValue(val) => val.as_instruction(),
                BasicValueEnum::VectorValue(val) => val.as_instruction()
            }
            _ => None
        }
    }
}

extern "C"  { fn get_name(isnt: InstructionValue) -> *const c_char; }

pub struct IR<'ctx>   {
    pub ir_map: HashMap<LLVMValue<'ctx>, usize>, //IR value to line number
    pub line_map: Vec<Option<LLVMValue<'ctx>>>, //line indexed array of IR values
    pub name_map: HashMap<String, LLVMValue<'ctx>> //SSA names to IR values
}

impl<'a, 'ctx> IR<'ctx> {

    pub fn new(lines: &'a Vec<String>, module: &Module<'ctx>) -> Self {
        let mut ir = IR { ir_map: HashMap::new(), line_map: vec![None; lines.len()], name_map: HashMap::new() };
        ir.parse_ir(lines, module as *const Module);
        return ir;
    }

    //TODO: get working with functions
    //use API to get SSA name so we know what to highlight
    pub fn get_uses(&self, val: &'ctx LLVMValue) -> Vec<&usize>   {
        let mut current_use = val.get_first_use();
        let mut line_numbers: Vec<&usize> = Vec::new();

        while current_use != None   {
            let user = current_use.unwrap().get_user();
            let user_inst = match user  {
                AnyValueEnum::FloatValue(v) => v.as_instruction(),
                AnyValueEnum::IntValue(v) => v.as_instruction(),
                AnyValueEnum::PointerValue(v) => v.as_instruction(),
                AnyValueEnum::StructValue(v) => v.as_instruction(),
                AnyValueEnum::ArrayValue(v) => v.as_instruction(),
                AnyValueEnum::VectorValue(v) => v.as_instruction(),
                AnyValueEnum::PhiValue(v) => Some(v.as_instruction()),
                AnyValueEnum::InstructionValue(v) => Some(v),
                AnyValueEnum::FunctionValue(_) => None,
                AnyValueEnum::MetadataValue(_) => None
            };
            if user_inst == None    {
                panic!("user is not an instruction");
            }
            let user_inst = LLVMValue::Instruction(user_inst.unwrap());
            line_numbers.push(self.ir_map.get(&user_inst).unwrap());
            current_use = current_use.unwrap().get_next_use();
        }

        return line_numbers;
    }

    //TODO: get working with function names and function args
    pub fn get_defs(&self, inst: &'ctx InstructionValue) -> Vec<&usize>   {
        (0..inst.get_num_operands())
            .filter(|n|
                    match inst.get_operand(*n).unwrap()  {
                        Either::Left(op) => if let Some(_) = match op    {
                                BasicValueEnum::ArrayValue(val) => val.as_instruction(),
                                BasicValueEnum::IntValue(val) => val.as_instruction(),
                                BasicValueEnum::FloatValue(val) => val.as_instruction(),
                                BasicValueEnum::PointerValue(val) => val.as_instruction(),
                                BasicValueEnum::StructValue(val) => val.as_instruction(),
                                BasicValueEnum::VectorValue(val) => val.as_instruction()
                            } { true } else { false },
                        Either::Right(_) => true
                    }
            )
            .map(|n|
                self.ir_map.get(
                    &match inst.get_operand(n).unwrap() {
                        Either::Left(op) => LLVMValue::Instruction(op.as_instruction_value().unwrap()),
                        Either::Right(op) => LLVMValue::BasicBlock(op)
                    }
                ).unwrap()
            )
            .collect::<Vec<_>>()
    }

    //TODO: once you have access to value refs implement basic blocks
    // also consider just moving this into a trait to get rid of the either enum if it doesn't need self
    fn get_preds(self, val: &Either<PhiValue<'ctx>, BasicBlock<'ctx>>) -> Vec<BasicBlock<'ctx>> {
        match val   {
            Either::Left(phi) => (0..phi.count_incoming()).map(|i| phi.get_incoming(i).unwrap().1).collect::<Vec<_>>(),
            Either::Right(block) => panic!("unimplemented")
        }
    }

    fn parse_insts(&mut self, i: usize, inst: InstructionValue<'ctx>)   {
        let value_ref = LLVMValue::Instruction(inst);
        self.line_map[i] = Some(value_ref);
        let value_ref = LLVMValue::Instruction(inst);
        self.ir_map.insert(value_ref, i);
        let ssa_name =  inst.get_name().unwrap().to_str().unwrap();
        let value_ref = LLVMValue::Instruction(inst);
        self.name_map.insert(String::from(ssa_name), value_ref);
        let next_inst = inst.get_next_instruction();
        if next_inst.is_none() { return }
        self.parse_insts(i+1, next_inst.unwrap())
    }

    //FIXME: handle single block function case
    fn parse_block(&mut self, i: usize, ssa_name: &'a str, current_blocks: &mut Vec<BasicBlock<'ctx>>) -> Option<InstructionValue<'ctx>>   {
        for block in current_blocks.iter() {
            let name = block.get_name().to_str().unwrap();
            if ssa_name == block.get_name().to_str().unwrap() {
                let value_ref = LLVMValue::BasicBlock(*block);
                self.line_map[i] = Some(value_ref);
                let value_ref = LLVMValue::BasicBlock(*block);
                self.ir_map.insert(value_ref, i);
                return block.get_first_instruction();
            }
        }
        panic!("Block name not found in module!");
    }

    fn parse_func(&mut self, module: *const Module<'ctx>, i: usize, ssa_name: &'a str) -> Vec<BasicBlock<'ctx>>    {
        let func = unsafe { module.as_ref().unwrap().get_function(ssa_name).unwrap() };
        let value_ref = LLVMValue::Function(func);
        self.line_map[i] = Some(value_ref);
        let value_ref = LLVMValue::Function(func);
        self.ir_map.insert(value_ref, i);
        return func.get_basic_blocks();
    }

    fn parse_ir(&mut self, lines: &'a Vec<String>, module: *const Module<'ctx>)   {
        let block = Regex::new(r#"^(".*"|(\w|\.)*):"#).unwrap();
        let func = Regex::new(r"^define ").unwrap();
        let func_name = Regex::new(r#"@(".*"|\w*)\("#).unwrap();
        let mut current_blocks: Vec<BasicBlock> = Vec::new();
        let mut in_block = false;
        let mut next_inst: Option<InstructionValue> = None;

        for (i, line) in lines.iter().enumerate()   {
            let line = line.trim_start_matches(|c: char| c.is_whitespace());
            if in_block {
                self.parse_insts(i, next_inst.unwrap());
                in_block = false;
            }
            else if let Some(name_bounds) = block.find(line)    {
                in_block = true;
                let a = name_bounds.start();
                let b = name_bounds.end()-1;
                let ssa_name = strip_quotes(a, b, line);
                next_inst = self.parse_block(i, ssa_name, &mut current_blocks);
            }
            else if func.is_match(line)  {
                let name_bounds = func_name.find(line).unwrap();
                let a = name_bounds.start()+1;
                let b = name_bounds.end()-1;
                let ssa_name = strip_quotes(a, b, line);
                current_blocks = self.parse_func(module, i, ssa_name);
            }
        }
    }
}

fn strip_quotes(a: usize, b: usize, line: &str) -> &str   {
    let mut ssa_name = &line[a..b];
    if ssa_name.find("\"").is_some() {
        ssa_name = &line[a+1..b-1];
    }
    ssa_name
}

// fn get_test_inst<'ctx>(module: &Module<'ctx>) -> InstructionValue<'ctx>   {
//     module.get_first_function()
//           .unwrap()
//           .get_first_basic_block()
//           .unwrap()
//           .get_last_instruction()
//           .unwrap()
//           .get_previous_instruction()
//           .unwrap()
// }

// fn get_test_block<'ctx>(module: &Module<'ctx>) -> BasicBlock<'ctx>   {
//     module.get_first_function()
//           .unwrap()
//           .get_first_basic_block()
//           .unwrap()
//           .get_next_basic_block()
//           .unwrap()
// }

// pub fn test<'ctx>(ir: &IR, module: &Module<'ctx>)	{
//     let inst = get_test_inst(module);
//     let basic_block = get_test_block(module);
//     let text_data = ir.ir_map.get(&LLVMValue::Instruction(inst)).unwrap();
//     println!("instruction: {}", text_data);
//     let text_data = ir.get_defs(&inst);
//     for data in text_data.iter()    {
//         println!("{}", data);
//     }
//     let val = ir.line_map[25].unwrap();
//     let inst = val.as_instruction().unwrap();
//     let text_data = ir.get_defs(&inst);
//     for data in text_data.iter()    {
//         println!("{}", data);
//     }
//     // let inst_value = LLVMValue::Instruction(inst);
//     // let block_value = LLVMValue::BasicBlock(basic_block);
//     // let line_numbers = ir.get_uses(&inst_value);
//     // line_numbers.iter().for_each(|n| println!("{}", n));
//     // let line_numbers = ir.get_uses(&block_value);
//     // line_numbers.iter().for_each(|n| println!("{}", n));
// }
